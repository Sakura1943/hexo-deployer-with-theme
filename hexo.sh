#!/bin/bash
# Hexo部署脚本
#
## 欢迎界面
#
## 函数
# 消息框
dialog_msgbox(){
	dialog --title "$1" --msgbox "$2" $3 $4
}

# 输入框
dialog_inputbox(){
	dialog --title "$1" --inputbox "$2" $3 $4
}

# 输入框 To File
dialog_toinputbox(){
	dialog --title "$1" --inputbox "$2" $3 $4 2>$5
}

# Yes/No
dialog_yesno(){
	dialog --title "$1" --no-shadow --yesno "$2" $3 $4
}

# Yes/No To File
dialog_toyesno(){
	dialog --title "$1" --no-shadow --yesno "$2" $3 $4 2>$5
}

# clear
dialog_clear(){
	dialog --clear
}

# exit
dialog_exit(){
	if [[ $? -eq 1 ]]; then
		exit
	fi
}

# 欢迎
dialog_msgbox "Welcome" "欢迎使用Hexo部署脚本" 10 20
# 判断系统版本并开始部署
if grep -Eqii "Arch*" /etc/issue /etc/*-release; then
    echo 'OS is Arch'
    dialog_yesno "Ask for github/gitee pages" "您是否已经开启Github/Gitee Pages" 20 40 
    	result1=$?
    	dialog --clear
    	if [[ $result1 -eq 0 ]]; then
        	dialog_yesno "Ask for repos" "您是否开启仓库" 10 30    
			result2=$?
			dialog_clear
			if [[ $result2 -eq 1 ]]; then
				dialog_msgbox "Stop" "进程已停止" 10 20
				dialog_clear
				exit
			elif [[ $result2 -eq 0 ]]; then
				dialog_msgbox "npm install" "installing" 10 20
	    		sudo pacman -Syu vim nano git wget curl tar gzip zip nodejs-lts-erbium npm --noconfirm 
        		dialog_msgbox "npm配置为CN源" "Configuring" 10 20
				dialog_clear
        		npm config set registry https://registry.npm.taobao.org

				dialog_msgbox "安装Hexo部署工具" "Hexo deployer config" 10 20
				dialog_clear
	    		npm install -g hexo-cli
				npm uninstall --save hexo-generator-category
				npm install --save hexo-renderer-jade hexo-generator-archive hexo-generator-category-enhance hexo-generator-feed hexo-generator-tag
				npm install --save hexo-prism-plugin
				rm -rf node_modules  package.json  package-lock.json
				dialog_msgbox "Git账户绑定" "进行绑定" 10 20
				dialog_toinputbox "Github/Gitee邮箱" "请输入你的Github/Gitee注册邮箱" 20 40 email
				dialog_clear
	    		email=$(cat email)
				rm -rf email
        		git config --global user.email $email
	    		dialog_toinputbox "Github/Gitee用户名" "请输入你的Github/Gitee用户名" 20 40 username
				dialog_clear
	    		username=$(cat username)
				rm -rf username
        		git config --global user.name $username        

        		dialog_msgbox "Hexo部署" "Hexo开始部署..." 10 20
				dialog_msgbox "基础文件抓取(From Git)" "基础文件开始抓取..." 10 20
				dialog_toinputbox "项目保存位置" "请输入项目保存的位置" 17 30 project
				dialog_clear
	    		project=$(cat project)
				rm -rf project
        		mkdir -p ~/$project && cd ~/$project && npm install hexo-deployer-git && wget https://mirrors.yaoyz.cn/hexo/hexo-start.tar && tar -xvf hexo-start.tar && npm install hexo-deployer-git
        		rm -r hexo-start.tar
        			# 信息读入临时文件

        		dialog_toinputbox "博客标题" "请输入你的博客标题" 17 20 title
				title=$(cat title)
				rm -rf title
				dialog_toinputbox "描述" "请输入博客描述" 17 20 description
				description=$(cat discription)
				rm -rf description
				dialog_toinputbox "作者" "请输入作者name" 17 20 author
				author=$(cat author)
				rm -rf author
				dialog_toinputbox "语言" "请输入你的显示语言(中文zh-Hans 英文en)" 20 25 language
				language=$(cat language)
				rm -rf language
				dialog_toinputbox "时区" "请输入时区(eg:Asia/Shanghai)" 20 25 timezone
				timezone=$(cat timezone)
				rm -rf timezone
				dialog_toinputbox "Git仓库地址" "请输入你的Git仓库地址" 20 25 repo
				repo=$(cat repo)
				rm -rf repo
				dialog_toinputbox "Git Pages提供的地址" "请输入Git Pages提供的地址" 20 25 url
				url=$(cat url)
				rm -rf url
				dialog_toinputbox "Pages解析目录" "请输入你的Git Pages服务根目录地址 eg:/" 20 25 root
				root=$(cat root)
				rm -rf root
				dialog_clear
				echo -e "# Hexo Configuration\n## Docs: https://hexo.io/docs/configuration.html\n## Source: https://github.com/hexojs/hexo/\n \n# Site\ntitle: $title\nsubtitle: ''\ndescription: $description\nkeywords:\nauthor: $author\nlanguage: $language\ntimezone: $timezone\n \n# URL\n## Set your site url here. For example, if you use GitHub Page, set url as 'https://username.github.io/project'\nurl: $url\nroot: $root\npermalink: :year/:month/:day/:title/\npermalink_defaults:\npretty_urls:\n  trailing_index: true # Set to false to remove trailing 'index.html' from permalinks\n  trailing_html: true # Set to false to remove trailing '.html' from permalinks\n \n# Directory\nsource_dir: source\npublic_dir: public\ntag_dir: tags\narchive_dir: archives\ncategory_dir: categories\ncode_dir: downloads/code\ni18n_dir: :lang\nskip_render:\n \n# Writing\nnew_post_name: :title.md # File name of new posts\ndefault_layout: post\ntitlecase: false # Transform title into titlecase\nexternal_link:\n  enable: true # Open external links in new tab\n  field: site # Apply to the whole site\n  exclude: ''\nfilename_case: 0\nrender_drafts: false\npost_asset_folder: false\nrelative_link: false\nfuture: true\nhighlight:\n  enable: false\n  line_number: true\n  auto_detect: false\n  tab_replace: ''\n  wrap: true\n  hljs: false\nprismjs:\n  enable: false\n  preprocess: true\n  line_number: true\n  tab_replace: ''\n \n \n# plugins\nplugin:\n  - hexo-generator-category-enhance\n  - hexo-generator-feed\n  - hexo-asset-image\n  - hexo-prism-plugin\n  - hexo-toc\n \narchive_generator:\n  per_page: 0\n \n \ncategory_generator:\n  per_page: 10\n  enable_index_page: true\n \n \ntag_generator:\n  per_page: 10\n  enable_index_page: true\n \n \n# Generator atom feed for you website\nfeed:\n  type: atom\n  path: atom.xml\n  limit: 20\n  hub:\n  content:\n  content_limit: 140\n  content_limit_delim: ' '\n \n \n# For syntax highlighting\nprism_plugin:\n  mode: 'preprocess'\n  theme: 'default'\n  line_number: true\n \n \n# Home page setting\n# path: Root path for your blogs index page. (default = '')\n# per_page: Posts displayed per page. (0 = disable pagination)\n# order_by: Posts order. (Order by date descending by default)\nindex_generator:\n  path: ''\n  per_page: 10\n  order_by: -date\n \n# Category & Tag\ndefault_category: uncategorized\ncategory_map:\ntag_map:\n \n \n# Metadata elements\n## https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta\nmeta_generator: true\n \n \n# Date / Time format\n## Hexo uses Moment.js to parse and display date\n# You can customize the date format as defined in\n# http://momentjs.com/docs/#/displaying/format/\ndate_format: YYYY-MM-DD\ntime_format: HH:mm:ss\n## updated_option supports 'mtime', 'date', 'empty'\nupdated_option: 'mtime'\n \n \n# Pagination\n## Set per_page to 0 to disable pagination\nper_page: 10\npagination_dir: page\n \n# Include / Exclude file(s)\n## include:/exclude: options only apply to the 'source/' folder\ninclude:\nexclude:\nignore:\n\n \n \n# Extensions\n## Plugins: https://hexo.io/plugins/\n## Themes: https://hexo.io/themes/\ntheme: typoography\n  \n# Deployment\n## Docs: https://hexo.io/docs/one-command-deployment\ndeploy:\n  type: git\n  repo: $repo\n  branch: master" > _config.yml
				dialog_toinputbox "主题显示名称" "请输入你在主题下显示的名称" 20 30 title1
				title1=$(cat title1)
				rm -rf title1
				dialog_toinputbox "标题简介Line1" "请输入标题简介" 20 30 title_primary
				title_primary=$(cat title_primary)
				rm -rf title_primary
				dialog_toinputbox "标题简介Line2" "请输入标题简介第二行" 20 30 title_secondary
				title_secondary=$(cat title_secondary)
				rm -rf title_secondary
				dialog_clear
				echo -e "title: $title1\ntitle_primary: $title_primary\ntitle_secondary: $title_secondary\nkeywords:\n \nemail:  #E-mail address\nbilibili: #bilibili UID\nsteam: #user name\ntwitter: #username\nrss: atom.xml\nweibo: #username/id\ninstagram: #username\ngithub: \n \n# Choose the comment service according to your need.\n# Please do not use the two services at the same time.\n#livere: # [data-uid] on livere.com\n#disqus: # [short_name] on disqus.com\n#dove: # For fun ONLY. Set to true to 'disable' comments on your site.\n \n# Set true to show the page indicator.\nshowPageCount: true\n \n# Set true to show category or tags behind post titles\nshowCategories: true\nshowTags: true\n \n# Color scheme\nthemeStyle: light # light/dark" > themes/typoography/_config.yml
				npm install --force
				hexo cl && hexo g -d
		   		 
				dialog_msgbox "手动更新" "请进入Git Pages页面手动更新" 15 20
				dialog_clear
			fi	
    	else
			dialog_msgbox "Error 请输入y/n" 10 20
			dialog_clear
    	fi
elif grep -Eqii 'Debian*' /etc/issue /etc/*-release;then
	dialog_msgbox "Error" "OS is Debian" 10 20
	dialog_msgbox "Error" "目前仅撰写Arch Linux的脚本" 15 20
else
	dialog_msgbox "Error" "目前仅撰写Arch Linux的脚本" 25 20
fi
